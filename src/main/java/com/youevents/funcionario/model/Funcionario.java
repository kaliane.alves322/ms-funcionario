package com.youevents.funcionario.model;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data // get e seters
public class Funcionario {
    @Id // primary key
    private String id;
    private String nome;
    private String endereco;
    private String email;
    private String telefone;
    private String cpf;
    private double salario;
    private String cargo;
    private String admissao;
    private String demissao;

}
