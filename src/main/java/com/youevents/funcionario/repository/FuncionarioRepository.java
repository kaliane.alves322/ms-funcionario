package com.youevents.funcionario.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.youevents.funcionario.model.Funcionario;

public interface FuncionarioRepository extends MongoRepository<Funcionario, String> {

}
