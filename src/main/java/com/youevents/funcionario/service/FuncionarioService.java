package com.youevents.funcionario.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.youevents.funcionario.dto.FuncionarioDTO;
import com.youevents.funcionario.mapper.FuncionarioMapper;
import com.youevents.funcionario.model.Funcionario;
import com.youevents.funcionario.repository.FuncionarioRepository;

@Service

public class FuncionarioService {

    @Autowired
    FuncionarioRepository repository;

    public FuncionarioDTO inserir(FuncionarioDTO dto) {
        // convertemos dto em model
        Funcionario funcionario = FuncionarioMapper.dtoToModel(dto);
        // para chamarmos no insert e receber o funcionario salvo
        Funcionario salvo = this.repository.insert(funcionario);
        // para depois converter o dado "salvo" que agora é model, em dto.
        FuncionarioDTO novo = FuncionarioMapper.modelToDto(salvo);
        // transformando em dto para devolver pro controler.
        return novo;

    }

    public List<FuncionarioDTO> listarTodos() {
        // Obter listagem do nosso banco de dados, todos item da lista.
        List<Funcionario> lista = this.repository.findAll();
        List<FuncionarioDTO> listaDto = new ArrayList<FuncionarioDTO>();
        for (var item : lista) {
            FuncionarioDTO novo = FuncionarioMapper.modelToDto(item);
            listaDto.add(novo);
        }
        return listaDto;
    }

    public FuncionarioDTO atualizar(String id, FuncionarioDTO dto) throws Exception {
        // buscar um id no banco de dados(repository)
        // verificou se existe um funcionario com id no bd.
        this.repository.findById(id)
                .orElseThrow(() -> new Exception("Funcionario não encontrado"));
        // convertemos de dto para model
        Funcionario novo = FuncionarioMapper.dtoToModel(dto);
        // setamos o id
        novo.setId(id);
        // executa o update
        // convertemos novamente só que de model para dto
        FuncionarioDTO atualizarDto = FuncionarioMapper.modelToDto(this.repository.save(novo));
        // retornamos para controller
        return atualizarDto;
    }

    public FuncionarioDTO buscar(String id) throws Exception {
        // buscou no banco de dados o id de tal funcionario
        Funcionario funcionario = this.repository.findById(id)
                .orElseThrow(() -> new Exception("Funcionario não encontrado"));
        // convertemos só para dto por que não inserimos nada no bd
        return FuncionarioMapper.modelToDto(funcionario);

    }

    public void delete(String id) {
        this.repository.deleteById(id);
    }

}
